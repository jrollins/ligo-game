VERSION = $(shell git describe  --tags --abbrev=0)
SVERSION = $(shell git describe  --tags --abbrev=7)
DOC = Observe!.pdf
VDOC = Observe_game_v$(VERSION).pdf

#DOCS = rules example tiles cards cheatsheet tilesheet
#DOCS = rules example tiles cheatsheet tilesheet
#DOCS = rules example tiles tableau tilesheet cardsheet_v6
DOCS = rules tilesheet cardsheet_v6
DOCS := $(addsuffix .pdf, $(DOCS))

TILES := \
	laser BS ETM PD TM \
	tube SUS SEI \
	IMC PRM ITM SRM OMC \
	AMP TCS
TILES_OUT = $(addprefix tiles/, $(addsuffix .pdf, $(TILES)))
CARDS := \
	scientist hardware-engineer computer-engineer operator manager
# CARDS := \
# 	scientist
CARDS_OUT = $(addprefix cards/, $(addsuffix .pdf, $(CARDS)))
ICONS := \
	grant rnd install commission observe publish
ICONS_OUT = $(addprefix icons/, $(addsuffix .pdf, $(ICONS)))
TABLEAUS := \
	tableau0 tableau1 tableau1r tableau2 tableau2r tableau3 tableau3r
TABLEAUS_OUT = $(addprefix pictures/, $(addsuffix .tex, $(TABLEAUS)))

DOC_FIGS := \
	pictures/michelson.tex pictures/drfpmi.tex \
	$(TABLEAUS_OUT) ochip.pdf

#SVG_EXTRACT = svg-export-layers
SVG_EXTRACT = svg-export

##################################################

.PHONY: all
all: $(DOC) full_detector.png

$(DOC): $(DOCS)
	pdftk $^ cat output $@
	rm -f Observe_game_*.pdf
	ln -sf $@ $(VDOC)

.PHONY: defs.tex
defs.tex: %: %.in
	sed "s/__VERSION__/$(SVERSION)/" $< >$@

.PHONY: rules cheatsheet
rules: %: %.pdf
.PHONY: rules.pdf
rules.pdf: %.pdf: %.tex defs.tex references.bib $(DOC_FIGS)
	pdflatex $(basename $<)
	bibtex $(basename $<)
	pdflatex $(basename $<)
	pdflatex $(basename $<)
# tiles.pdf: %.pdf: %.tex defs.tex $(TILES_OUT)
# 	pdflatex $<
# 	pdflatex $<
# 	pdflatex $<
# cards.pdf: %.pdf: %.tex defs.tex $(CARDS_OUT)
# 	pdflatex $<
# 	pdflatex $<
# 	pdflatex $<
# example.pdf: %.pdf: %.tex defs.tex $(TABLEAUS_OUT) ochip.pdf
# 	pdflatex $<
# 	pdflatex $<
# 	pdflatex $<
cheatsheet.pdf: %.pdf: %.tex defs.tex #pictures/michelson.tex pictures/drfpmi.tex
	pdflatex $<
	pdflatex $<
	pdflatex $<

.PHONY: tiles
tiles: $(TILES_OUT) tilesheet.pdf
$(TILES_OUT): detector2.svg
	mkdir -p tiles
	./$(SVG_EXTRACT) -i -d -o $@ $< label=tile:$(basename $(notdir $@))
# tiles.pdf: art.svg $(SVG_EXTRACT)
# 	./$(SVG_EXTRACT) -p -o $@ $< layer='tile sheet'
#./$(SVG_EXTRACT) -p $< $@ layer='tile sheet'
#inkscape art.svg --export-area-page --export-pdf=tiles.pdf 2>/dev/null
tilesheet.pdf: %.pdf: %.tex defs.tex $(TILES_OUT)
	pdflatex $<
	pdflatex $<
	pdflatex $<

.PHONY: cards
cards: $(CARDS_OUT)
$(CARDS_OUT): personnel.svg
	mkdir -p cards
	./$(SVG_EXTRACT) -i -d -o $@ $< label=$(basename $(notdir $@))

.PHONY: icons
icons: $(ICONS_OUT)
$(ICONS_OUT): icons.svg
	mkdir -p icons
	./$(SVG_EXTRACT) -i -d -o $@ $< label=icon:$(basename $(notdir $@))
ochip.pdf: %.pdf: %.svg
	inkscape --export-pdf=$@ --export-area-drawing $<


tableau.pdf: %.pdf: %.tex defs.tex
	pdflatex $<
tableau.svg: %.svg: %.pdf
	pdf2svg $< $@
tableau.png: %.png: %.svg
	inkscape --export-png=$@ --export-dpi=150 --export-area-drawing $<
full_detector.pdf: %.pdf: %.tex defs.tex
	pdflatex $<
full_detector.svg: %.svg: %.pdf
	pdf2svg $< $@
full_detector.png: %.png: %.svg
	inkscape --export-png=$@ --export-dpi=150 --export-area-drawing $<

# .PHONY: figs
# figs: $(FIGURES)
# personnel.pdf: art.svg $(SVG_EXTRACT)
# 	./$(SVG_EXTRACT) -d -o $@ $< layer='personnel cards'
# dtech.pdf: art.svg $(SVG_EXTRACT)
# 	./$(SVG_EXTRACT) -d -o $@ $< layer='dtech cards'
# tableau.pdf: art.svg $(SVG_EXTRACT)
# 	./$(SVG_EXTRACT) -d -o $@ $< layer='tableau'
# michelson.pdf: art.svg $(SVG_EXTRACT)
# 	./$(SVG_EXTRACT) -d -o $@ $< layer='simple michelson'
#inkscape art.svg --export-area-drawing --export-id=g5768 --export-pdf=michelson.pdf 2>/dev/null


.PHONY: refs
refs: references.bib
# NOTE: should add the following to full author list entries:
#   author = "LIGO{\ }Scientific{\ }Collaboration",
references.bib:
	xapers bibtex tag:glory-points > $@


.PHONY: clean
clean:
	rm -f *.aux *.bbl *.blg *.log *.out *.toc *.vrb *.snm
