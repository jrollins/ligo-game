#!/usr/bin/env python

import math
import numpy as np
import matplotlib.pyplot as plt

##########

from scipy.stats import powerlaw

power = 3
# number cards, minus one for the 4
N = 6*9 - 1
Dmax = 256

bins = np.arange(8, Dmax+4, 8)
#bins = 2**np.arange(3, 9)
#print bins

# a * x**(a-1)
a = power + 1
s = powerlaw.rvs(a, size=N)*(Dmax)

(n, bins, patches) = plt.hist(s, bins=bins)

fmt = '%3.f %3.f'
print fmt % (1,4)
for i,b in zip(n,bins):
    if i != 0:
        print fmt % (i,b)
#print sum(n)

x = np.arange(100)/100.
#plt.plot()
# exp = np.arange(2, 9)
# R = 2**exp
# N = ((R/4))

# plt.loglog(R, N)

plt.show()

# for i in range(256):
#     if i%8 == 0:
#         print i

exit()

##########
##########

class ITM():
    img = '['
    cost = 3

R = np.arange(0, 1000000, dtype=float)

log = np.log2

#S = np.log(r) * 3
lR = log(R)
#lV = log(R**3)
lV = 3*log(R) - 5

#N = (r)**1.5

fig, ax1 = plt.subplots()
ax1.semilogx(R, lR, 'b')
#ax1.set_ylim(10**-3, 10**3)
ax1.grid('on')
ax1.set_ylabel("log(R)", color='b')

ax2 = ax1.twinx()
ax2.semilogx(R, lV, 'r')
#ax2.loglog(r, N, 'r')
ax2.set_ylim([-5, 55]) #np.array(ax2.get_ylim()) + 5)
ax2.set_ylabel("log(V) = 3 log(R)", color='r')

#
ax1.axhline(2, color='g')
ax1.axhline(10, color='g')
ax1.axhline(20, color='g')

ax1.set_xlabel("R [kpc]")

plt.show()
