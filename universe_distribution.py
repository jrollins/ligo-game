#!/usr/bin/env python

import sys
import numpy as np
from scipy.stats import powerlaw
import matplotlib.pyplot as plt

try:
    ncards = int(sys.argv[1])
except IndexError:
    sys.exit("usage: %s ncards [power [Dmax]]")

try:
    power = int(sys.argv[2])
except IndexError:
    power = 3

try:
    Dmax = int(sys.argv[3])
except IndexError:
    Dmax = 200

# number cards, minus one for the 4
N = ncards - 1
bsize = 8

# a * x**(a-1)
a = power + 1
dist = powerlaw.rvs(a, size=N)*(Dmax+bsize)
#print len(dist), max(dist)

bins = np.arange(8, Dmax+2*bsize, bsize)
(n, b, patches) = plt.hist(dist, bins=bins)
# print bins
# print n

print "cards: %s" % ncards
print "distribution: x**%s" % power
print

fmt = '%3.f  %2.f'

print 'Mpc   N'
print '---  --'
print fmt % (4,1)
for i,b in zip(n,bins):
    if i != 0:
        print fmt % (b,i)
#print sum(n)

x = np.arange(100)/100.

#plt.show()
