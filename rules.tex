\documentclass[prl,twocolumn,superscriptaddress,letterpaper,nofootinbib,nopreprintnumbers,showpacs]{revtex4-1}

%\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
  colorlinks=true,
  pdfstartview=FitV,
  linkcolor=blue,
  citecolor=magenta,
  urlcolor=red
}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{rotating}
\usepackage[usenames,dvipsnames]{color}
%\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{epsdice}
\usepackage{tabularx}
%\usepackage{array}
\usepackage[export]{adjustbox}

% \pagestyle{fancy}
% \fancyhead[]{}
% \fancyfoot[CE,CO]{v\version}
% \renewcommand{\headrulewidth}{0pt}
% \renewcommand{\footrulewidth}{1pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{defs}

\newcommand{\defskip}[0]{\vspace*{-1.3em}}
\newcommand{\phasesec}[2]{\bigskip\noindent\textbf{#1}:\ {#2}\vspace*{0.5em}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{
  \gname \\
  A gravitational-wave detection board game
  % \\
  % (version: \version)
}

\author{Jameson Graef Rollins and Michael H. Wong}

%\date{\version}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}
  This article describes a board game based on the quest to detect
  gravitational waves.  Two or more players compete to detect the most
  gravitational waves by hiring scientists and engineers, researching
  detector technology, and building the most sensitive ground-based
  interferometric gravitational-wave detector.  Players must balance
  detector upgrades and observing runs so as to maximize their
  detection potential, and be the first to detect the elusive
  gravitational wave.
\end{abstract}

\maketitle
\thispagestyle{fancy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

In 1916, Albert Einstein realized that his new theory of General
Relativity predicted the existence of waves of gravity that would
travel through space at the speed of light~\cite{}.  Since the 1960's,
scientists of been trying to detect these waves.  They started by
building ``resonant mass'' detectors to see if they could detect these
waves of gravity coming from outer space~\cite{}.  By the 1990's, they
were building large and sophisticated ``laser interferometer''
detectors that had much higher sensitivity~\cite{} to these waves, and
could therefore potentially detect waves coming from much farther away
in the Universe.

Gravitational waves could be emitted by some of the most energetic and
fascinating events in the Universe.  Stars exploding or black holes
colliding all potentially produce gravitational waves that could be
detected by these new interferometric detectors.  If scientists can
detect these waves, there are so many new and exciting things that can
be learned about the Universe.

%We will never stop listening for gravitational waves!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Game description}

In this game, players compete to detect gravitational wave events and
collect the most prestige.  Each player builds an interferometer
detector, hires staff to build and operate the detector, and
researches technology to improve detection sensitivity.  Each round,
players may either improve their detector, or use the detector to
observe the Universe, but not both.  If a player chooses to observe, a
\universe\ card is revealed.  If multiple players observe at the same
time, the chances of detection increase for everyone, so it's better
for everyone to work together.  If the player's detector sensitivity
is high enough, they may detect the event and earn detection prestige.
The player with the most detection prestige at the end of the game
wins.

The components of the game are:
\begin{itemize}[noitemsep]
\item detector tiles
\item 1 deck of \resource\ cards
\item 1 deck of \universe\ cards
\item 1 funding counter per player
\item 1 \Ochip\ per player
%\item 1 observatory die
\item commissioning chips
\item prestige chips
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Detector}

Each player builds a ground-based interferometric gravitational wave
detector~\cite{Aasi_2015} made up of individual detector component
tiles.  The individual detector tiles represent particular components
of a detector, such as lasers, mirrors, photodetectors, beam tubes,
suspension stages, etc.  They are assembled together by placing them
adjacent to each other on the table in front of each player.  An
example of the initial detector can be seen in Fig.~\ref{f:michelson},
and a ``complete'' detector can be seen in Fig.~\ref{f:DRFPMI}.

% \begin{figure}[t]
% \centering
% \includegraphics[width=0.5\columnwidth]{michelson.pdf}
% \caption{
%   \label{f:michelson} A ``simple Michelson'' initial detector
%   configuration, worth 6 Mpc.}
% \end{figure}
\begin{figure}[t]
\setlength{\unitlength}{0.06in}
\setlength{\tilesize}{9\unitlength}
\begin{picture}(30,37)(-10,-10)
\input{pictures/michelson}
\end{picture}
\caption{
  \label{f:michelson} A ``simple Michelson'' initial detector
  configuration, worth 6 Mpc.  The brown cards represent the laser and
  optical components of the detector, while the gray ``TM'' card
  represents the first stage in a suspension system for the optics.}
\end{figure}

The overall sensitivity of a detector is the sum of the ``Mpc'' value
of all individual tiles in a detector.  The initial ``simple
Michelson'' detector has a value of 6 Mpc.

There are rules about how the detector is assembled.  The restrictions
about how individual tiles can be placed is described in the
``Detector Tiles'' appendix.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Resource cards and the Tableau}
\label{s:resources}

The \resource\ deck consist of two types of cards:
\textit{\personnel}\ and \textit{\dtech}.  \resource\ cards are
acquired via funding, either during the Funding phase or when a
Scientist or Manager writes a grant during the Assignment phase.  See
the Phases section for more information.

Each player keeps their \resource\ cards in their hand.  When cards
are ``played'', they are placed into the players tableau, i.e. on the
table in front of them, below their detector.  See the Example Game
Play appendix for examples of what a tableau looks like.

\resource\ cards in a players hand (\textit{not} in their tableau) can
also be used as cash to pay for personnel salary, research and
development, and detector upgrades.  Each \resource\ card is worth
\$1, regardless of type.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Universe cards}
\label{s:universe}

The \universe\ deck consists of cards representing gravitational wave
events in the Universe.  Each card represents a particular type of
event at a specific amplitude-normalized distance.  The distance to a
particular event is given by the ``Mpc'' value in the upper right
corner of the card.

\universe\ cards are revealed during the \textit{detection} phase of
the game, when observing players have an opportunity to detect them.
Each event has a prestige value that is granted upon detection of the
event.  See the Observations sections for more information on event
detection.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Setup}

Distribute to each player the following:

\begin{itemize}[noitemsep]

\item 1 Scientist card pulled from the \resource\ deck

\item The following detector tiles:
  \begin{itemize}[noitemsep]
  \item 1 laser
  \item 1 beam splitter
  \item 2 end mirrors
  \item 1 photo-detector
  \item 1 test mass
  \end{itemize}

\item 1 \Ochip

\item 1 funding counter die

\end{itemize}

Each player arranges their detector tiles into a ``simple Michelson''
configuration as shown in Fig.~\ref{f:michelson}.  Place the \Ochip\ 
next to the detector.

The funding counters should be placed in front of each player with six
($\epsdice{6}$) facing up.

Shuffle the remaining \resource\ cards and the \universe\ cards and
place them in separate face-down stacks in the center of the playing
area.

Put the detector tiles, commissioning chips, and prestige points in
separate piles in the center of the playing area.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \begin{figure*}[ht!]
% \centering
% %\includegraphics[width=1\textwidth]{figures/strain-waveforms_v2.pdf}
% \includegraphics[width=0.7\textwidth]{tableau.pdf}
% \caption{
%   \label{f:DRFPMI} A ``full'' player tableau, featuring a detector
%   composed of at least one of every detector component type.  The
%   insert in the upper right shows the ``simple Michelson'' initial
%   detector configuration.}
% \end{figure*}
\begin{figure*}[ht!]
\setlength{\unitlength}{0.05in}
\setlength{\tilesize}{9\unitlength}
\begin{picture}(70,90)(-30,-30)
\input{pictures/drfpmi}
\end{picture}
\caption{
  \label{f:DRFPMI} A ``full'' detector, composed of at least one of
  every detector component type.}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Phases}

Each round consists of five phases: Funding, Hiring, Salary,
Assignment, and Resolve.  The phases are played in order, each played
simultaneously by all players.

%\bigskip\noindent\textit{Observation} ---
% \begin{description}[leftmargin=0cm]

% \item[1-Funding] \hfill \\
%\subsection{1 -- Funding}
\phasesec{1. Funding}{receive funding cards}

Draw \resource\ cards into hand corresponding to the number currently
shown on the funding counter.  Once \resource\ cards have been drawn,
all players must decrement their funding counter by one.  The counter
bottoms-out at one ($\epsdice{1}$), so each player will have the
opportunity to draw at least one funding card during this phase.  For
instance, if the funding counter shows five ($\epsdice{5}$) the player
takes five \resource\ cards from the deck, and then turns the counter
to show four ($\epsdice{4}$).

%   Players must stop drawing \resource\
% cards once they reach 10 cards in hand.

%\item[2-Hiring]  \hfill \\
%\subsection{2 -- Hiring}
\phasesec{2. Hiring}{hire personnel}

Play any number of \personnel\ cards from hand face up into tableau.

\textit{Poaching:} \personnel\ may be poached from other players.  If
a player wishes to poach, they must declare a ``signing bonus'' for
the desired personnel.  The player holding the desired personnel may
opt to either pay the bonus to the bank or release the personnel.  If
the personnel is released, the poaching player must pay the signing
bonus cost, at which point the personnel immediately enters their
tableau.  The poaching player may not declare a signing bonus larger
than they can afford to pay.

%\item[3-Salary]  \hfill \\
%\subsection{3 -- Salary}
\phasesec{3. Salary}{pay personnel}

Discard one \resource\ card from hand for each \personnel\ card in
tableau.  \personnel\ cards in tableau may be discarded in lieu of
payment.  There must always be at least one Scientist in tableau.

%\item[4-Assignment]  \hfill \\
%\subsection{4 -- Assignment}
\phasesec{4. Assignment}{assign actions to personnel}

Each unassigned \personnel\ may be assigned one action.  The available
actions are:

  \begin{description}[labelindent=1\parindent,leftmargin=2\parindent]

  \item[Grant write] generate additional funding \\
    \defskip

    Take one card from the \resource\ deck and place it face down on
    personnel.

    \textit{Only one grant may be submitted per round.}

  \item[R\&D] research new technology \\
    \defskip

    Place one \dtech\ card from hand onto assigned personnel, pay
    required research cost and place specified number of commissioning
    chips on the new \dtech\ card.

    Note that some personnel are limited in which kinds of technology
    they can research.  For instance, Hardware Engineers can only R\&D
    hardware technology, and Computer Engineers can only R\&D computer
    technology.

  \item[Install] add new components to detector \\
    \defskip

    Retrieve desired detector component tile (ones for which the
    required technology is available), place the new component tile
    with the specified number of commissioning chips on assigned
    personnel, and pay component cost from hand.

    Multiple components may be installed in the same round, assuming
    player has enough personnel and can cover the cost.  Some
    personnel may even install multiple components at once, and/or
    with reduced cost.

    If a player does not have the required technology for the desired
    component, they may use \dtech\ from another player by paying that
    player \$1 from hand and adding one additional commissioning chip
    on the tile.

    If the \Ochip\ is on the detector, remove it and place it aside.

    \textit{May not install if there are any un-published detections
      on the detector.}

  \item[Commission] commission detector or technology \\
    \defskip

    Remove one \Cchip\ from a detector tile or from a \dtech\ card in
    tableau and place it on assigned personnel.

    If the assigned personnel is capable of removing multiple \Cchip s
    during a given round, they may do so from different locations
    (e.g. a Scientist may remove one \Cchip\ from two different
    detector components, or from a detector component and a \dtech\
    card.

  \item[Observe!] look for gravitational waves \\
    \label{act:observe}
    \defskip

    Place the \Ochip\ O-side up on assigned personnel.  If this is the
    players first observation after an install, reset funding counter
    to 6 ($\epsdice{6}$).

    \textit{May not observe during the same round as install, or if
      there are any \Cchip s present on the detector.}

  \item[Analyze/Publish] extract detections from detector \\
    \label{act:publish}
    \defskip

    Retrieve one or more detection prestige from the detector onto
    assigned personnel, add one additional prestige to the retrieved
    stack, and reset funding counter to 6 ($\epsdice{6}$).

    Multiple detection prestige may be published either individually
    or as a group.  However, only one additional prestige is added per
    published stack.  So for instance, if the detector currently holds
    three detection prestige, all three prestige can be published
    together, for a total of four prestige after publication.
    Alternatively, they can be broken up into two or three different
    publications each receiving one additional prestige, for a total
    of either five or six prestige after they're all published.  Each
    individual publication is its own action, though, so publishing
    the stack as three separate publications would require the use of
    three Analyze/Publish actions.

  \end{description}

%\item[5-Universe]  \hfill \\
%\subsection{5--Universe}
\phasesec{5. Resolve}{resolve all assignments and reveal \universe\ cards}

Clear personnel of any actions items they have collected:
\begin{itemize}
\item Collect \resource\ cards from grant writers.
\item Move new \dtech\ cards from R\&D into tableau.
\item Move new detector component tiles with their commissioning chips
  into the appropriate place in the detector (see Fig.~\ref{f:DRFPMI}
  for appropriate tile placement).
\item Discard commissioned commissioning chips from commissioners.
\item Move the \Ochip\ onto detector and observe.  See
  section~\ref{s:observe}.
\item Move analyzed detection prestige into prestige pile.
\end{itemize}

If at the end of the Resolve phase any player has more than 10 cards
in their hand, they must discard down to 10.

%\end{description}

% \begin{figure*}[ht!]
% \setlength{\unitlength}{0.05in}
% \setlength{\tilesize}{9\unitlength}
% \setlength{\cardsize}{2\tilesize}
% \begin{picture}(70,90)(-30,-60)
% \input{pictures/tableau0}
% \end{picture}
% \caption{}
% \end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{s:observe}
\section{Observations}

Whenever one or more player observes during a round~\ref{act:observe},
\universe\ cards are revealed and each observing player determines if
they have made a detection.  One \universe\ card is revealed for each
observing player, and all players may observe all revealed cards.

Before \universe\ cards are revealed, each player calculates their
detector sensitivity.  Detector sensitivity is determined by adding
the ``Mpc'' (``mega-parsec'') values of each tile in a players
detector.  The Mpc value is indicated in the upper right corner of the
tile.  The final value for detector sensitivity is a number in units
of Mpc.

Each observing player compares their detector sensitivity in Mpc to
the Mpc distance on each of the revealed \universe\ cards.  If
detector sensitivity is greater than or equal to the event distance
then the player makes a detection.  A players \dtech cards may modify
the detectability of certain events, by e.g. reducing their effective
distance.

Each event includes a prestige value.  If a player detects an event,
they receive prestige equal to that event's prestige value.  If
multiple players detect the event, each player receives the specified
detection prestige.  The first detection of the game is worth twice
the prestige.

Place the detection prestige on the detector.  To remove the prestige
chips from the detector you must use the Analyse/Publish action
(see~\ref{act:publish}).  \textit{New detector components can not be
  installed in the detector until all prestige have been removed from
  the detector.}

\textit{Advanced Automation} - If any of the observing players has a
fully commissioned Advanced Automation \dtech\ card in their tableau,
and additional event is revealed after all other events have been
revealed.  Only players who have Advanced Automation may observe this
event.  Detection of this event work the same as other events.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Game resolution}

The game end is initiated when an event of distance \GAMEENDMPC\ or
more is detected by an observing player.  Once such an event is
detected, one more round is played, after which all players tally
their prestige.  Each prestige chip is worth one point.  The player
with the most prestige wins the game.

% Do \textit{not} count ``un-analyzed'' detections still on detector.
% Each analyzed detection chip is worth \DETECTPRESTIGE\ prestige, while
% each prestige chip is worth one prestige.  Add any bonus prestige
% described in the next section.  

% \subsection{Bonuses}

% Bonus prestige may be acquired at the end of the game for any of the
% following conditions:

% \begin{description}[labelindent=1\parindent,leftmargin=2\parindent]

% \item[Longest arms] \textcolor{red}{2}

% \item[Tallest suspension] \textcolor{red}{2}

% \item[Most powerful laser] \textcolor{red}{2}

% % \item[Most sensitive detector] \textcolor{red}{2}

% % \item[Most EM observations] \textcolor{red}{2}

% \end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Credits}

Many thanks to the following people for helping play test the game,
and for providing invaluable feedback and suggestions for improvement:
Rebecca Bureau, Darly Chou, Simone Chou, Dawn Garcia, Daniel Kahn
Gillmor, Christine Halvorson, Arthur Jones, John Kusakabe, Ken Macy,
Kerry McLaughlin, Larry Price, Nicolas Smith.  Particular thanks goes
to Ken Macy for his wonderful personnel drawings.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{references}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\onecolumngrid

\newpage
\section{Appendix: Example game play}
\input{example}

\newpage
\section{Appendix: Detector tiles}
\input{tiles}

\newpage
\section{Appendix: DIY game}
\input{diy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
