OBSERVE!
========

A gravitational-wave detection board game
-----------------------------------------

Observe! is a board game based on the quest to detect gravitational
waves.  Two or more players compete to detect the most gravitational
waves by hiring scientists and engineers, researching detector
technology, and building the most sensitive ground-based
interferometric gravitational-wave detector.  Players must balance
detector upgrades and observing runs so as to maximize their detection
potential, and be the first to detect the elusive gravitational wave.

This repository includes game rules, as well as all card, tile, and
other game piece art.  The main document product, with all necessary
components to play a DIY version of the game is:

    Observe!.pdf

To make the document just run "make" is the top level directory.
latex and python are required.

![](tableau.png)
