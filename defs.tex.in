\newcommand{\gname}[0]{Observe!}
\newcommand{\version}[0]{__VERSION__}

%%%%%%

% https://www.sharelatex.com/learn/Headers_and_footers
\usepackage{fancyhdr}
\pagestyle{fancy}
%\fancyhf{}
\fancyhead[]{}
\fancyfoot[LE,LO]{Observe!}
\fancyfoot[RE,RO]{v\version}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}

%%%%%%

\newcommand{\dcc}[1]{\href{http://dcc.ligo.org/#1}{LIGO-#1}}

\newcommand{\fixme}[1]{\textcolor{red}{\textbf{\textit{{#1}}}}}
\newcommand{\future}[1]{#1}
%\newcommand{\vodeck}[1]{\textcolor{Brown}{#1\ }}
\newcommand{\vodeck}[1]{}
\newcommand{\pc}[1]{\textcolor{red}{#1\ }}

% observation chip
\newcommand{\Ochip}[0]{observation chip}
% observation chip
\newcommand{\Cchip}[0]{commissioning chip}

% Resource deck
\newcommand{\resource}[0]{\texttt{Resource}}
%\newcommand{\resource}[0]{resource}
% personnel cards
\newcommand{\personnel}[0]{personnel}
% detector tech cards
\newcommand{\dtech}[0]{detector tech}

% Universe cards
\newcommand{\universe}[0]{\texttt{Universe}}

% personnel skills
\newcommand{\skill}[1]{\textbf{#1}}
\newcommand{\Ss}[0]{\skill{S}}
\newcommand{\Ms}[0]{\skill{M}}
\newcommand{\Rs}[0]{\skill{R}}
\newcommand{\Ds}[0]{\skill{D}}
\newcommand{\Os}[0]{\skill{O}}

\newcommand{\cd}[1]{\${#1}}
\newcommand{\cc}[1]{#1c}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MACROS
\newcommand{\macro}[1]{\textcolor{red}{#1}} % evidence in the text numbers from macros
\newcommand{\GAMEENDMPC}{\macro{100 Mpc}}

\newcommand{\DETECTPRESTIGE}{\macro{10}}

\newcommand{\BONUSARM}{\macro{15}}
\newcommand{\BONUSSUS}{\macro{15}}
\newcommand{\BONUSLASER}{\macro{15}}
\newcommand{\BONUSSENSE}{\macro{15}}
\newcommand{\BONUSMOST}{\macro{15}}
\newcommand{\BONUSEM}{\macro{15}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for detector drawings
\newlength{\tilesize}
\newlength{\cardsize}
